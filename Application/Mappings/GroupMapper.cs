﻿using Application.ViewModels;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Mappings
{
    public class GroupMapper
    {
        public GroupViewModel MapGroupToGroupViewModel(Group group)
        {
            return new GroupViewModel()
            {
                Id = group.Id,
                Name = group.Name
            };
        }
        public Group MapGroupViewModelToGroup(GroupViewModel group)
        {
            return new Group()
            {
                Id = group.Id ?? Guid.NewGuid(),
                Name = group.Name
            };
        }
        public GroupsListViewModel MapGroupsListToGroupsListViewModel(IEnumerable<Group> users)
        {
            return new GroupsListViewModel()
            {
                Groups = users.Select(x => MapGroupToGroupViewModel(x))
            };
        }
    }
}
