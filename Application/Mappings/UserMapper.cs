﻿using Application.ViewModels;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Mappings
{
    public class UserMapper
    {
        public UserViewModel MapUserToUserViewModel(User user)
        {
            return new UserViewModel()
            {
                Id = user.Id,
                Username = user.Username,
                Email = user.Email,
                DateUserCreated = user.DateUserCreated
            };
        }

        public UsersListViewModel MapUsersListToUsersListViewModel(IEnumerable<User> users)
        {
            return new UsersListViewModel()
            {
                Users = users.Select(x => MapUserToUserViewModel(x))
            };
        }

        public User MapUserViewModelToUser(UserViewModel user)
        {
            return new User()
            {
                Id = user.Id ?? Guid.NewGuid(),
                Username = user.Username,
                Email = user.Email,
                DateUserCreated = user.DateUserCreated ?? DateTime.Now
            };
        }
    }
}
