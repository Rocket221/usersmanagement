﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels
{
    public class UserViewModel
    {
        public Guid? Id { get; set; }
        [Required]
        public string Username { get; set; }
        public string Email { get; set; }
        public DateTime? DateUserCreated { get; set; }

    }
}
