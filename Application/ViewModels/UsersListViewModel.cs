﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels
{
    public class UsersListViewModel
    {
        public IEnumerable<UserViewModel> Users { get; set; }

    }
}
