﻿using Application.Mappings;
using Application.ViewModels;
using Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ApplicationServices
{
    public class GroupApplicationService
    {
        protected readonly GroupService groupService;
        protected readonly GroupMapper groupMapper;

        public GroupApplicationService(GroupService service, GroupMapper mapper)
        {
            groupService = service;
            groupMapper = mapper;
        }

        public GroupViewModel Get(Guid id)
        {
            return groupMapper.MapGroupToGroupViewModel(groupService.Get(id));
        }

        public GroupsListViewModel GetAll()
        {
            return groupMapper.MapGroupsListToGroupsListViewModel(groupService.GetAll());
        }

        public void Add(GroupViewModel user)
        {
            groupService.Save(groupMapper.MapGroupViewModelToGroup(user));
        }

        public void Delete(Guid id)
        {
            groupService.Delete(id);
        }
    }
}
