﻿using Application.Mappings;
using Application.ViewModels;
using Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ApplicationServices
{
    public class UserApplicationService
    {
        protected readonly UserService userService;
        protected readonly UserMapper userMapper;

        public UserApplicationService(UserService service, UserMapper mapper)
        {
            userService = service;
            userMapper = mapper;
        }

        public UserViewModel Get(Guid id)
        {
            return userMapper.MapUserToUserViewModel(userService.Get(id));
        }

        public UsersListViewModel GetAll()
        {
            return userMapper.MapUsersListToUsersListViewModel(userService.GetAllUsers());
        }

        public void Add(UserViewModel user)
        {
            userService.Save(userMapper.MapUserViewModelToUser(user));
        }

        public void Delete(Guid id)
        {
            userService.Delete(id);
        }
    }
}
