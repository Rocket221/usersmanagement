﻿using Domain.Promises;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class Repository<T> : IRepository<T> where T : class, IModelBase
    {
        protected readonly DbContext _context;

        public Repository(DbContext context)
        {
            _context = context;
        }

        public T Get(Guid id)
        {
            return _context.Set<T>().Find(id);
        }

        public IEnumerable<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }

        public void Save(T entity)
        {
            if (!_context.Set<T>().Any<T>(m => (m.Id == entity.Id)))
            {
                _context.Set<T>().Add(entity);
            }
            else
            {
                var currentEntity = this.Get(entity.Id);
                _context.Entry(currentEntity).CurrentValues.SetValues(entity);
            }
        }

        public void Remove(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

    }
}
