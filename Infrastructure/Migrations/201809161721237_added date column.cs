namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addeddatecolumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "DateUserCreated", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "DateUserCreated");
        }
    }
}
