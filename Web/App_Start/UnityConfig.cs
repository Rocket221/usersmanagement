using Application.ApplicationServices;
using Application.Mappings;
using Domain.Promises;
using Domain.Services;
using Infrastructure;
using System.Data.Entity;
using System.Web.Mvc;
using Unity;
using Unity.Lifetime;
using Unity.Mvc5;

namespace Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            //DbContext
            container.RegisterType<DbContext, SqlDbContext>(new TransientLifetimeManager());

            //Repository
            container.RegisterType(typeof(IRepository<>), typeof(Repository<>), new TransientLifetimeManager());

            //Services
            container.RegisterType<UserService, UserService>(new TransientLifetimeManager());
            container.RegisterType<GroupService, GroupService>(new TransientLifetimeManager());

            //ApplicationServices
            container.RegisterType<UserApplicationService, UserApplicationService>(new TransientLifetimeManager());
            container.RegisterType<GroupApplicationService, GroupApplicationService>(new TransientLifetimeManager());

            //Mappers
            container.RegisterType<UserMapper, UserMapper>(new TransientLifetimeManager());
            container.RegisterType<GroupMapper, GroupMapper>(new TransientLifetimeManager());

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}