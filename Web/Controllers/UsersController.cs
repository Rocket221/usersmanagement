﻿using Application.ApplicationServices;
using Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class UsersController : Controller
    {
        protected readonly UserApplicationService userService;

        public UsersController(UserApplicationService service)
        {
            userService = service;
        }

        public ActionResult Index()
        {
            return View(userService.GetAll());
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View("Form");
        }

        [HttpPost]
        public ActionResult Add(UserViewModel userViewModel)
         {
            if (!ModelState.IsValid)
            {
                return View("Form", userViewModel);
            }

            userService.Add(userViewModel);

            return RedirectToAction("Index");
        }

        public ActionResult Delete(Guid id)
        {
            userService.Delete(id);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var userViewModel = userService.Get(id);

            return View("Form", userViewModel);
        }

    }
}