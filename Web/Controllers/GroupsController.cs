﻿using Application.ApplicationServices;
using Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class GroupsController : Controller
    {
        protected readonly GroupApplicationService groupService;

        public GroupsController(GroupApplicationService service)
        {
            groupService = service;
        }

        public ActionResult Index()
        {
            return View(groupService.GetAll());
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View("Form");
        }

        [HttpPost]
        public ActionResult Add(GroupViewModel groupViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("Form", groupViewModel);
            }

            groupService.Add(groupViewModel);

            return RedirectToAction("Index");
        }

        public ActionResult Delete(Guid id)
        {
            groupService.Delete(id);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var userViewModel = groupService.Get(id);

            return View("Form", userViewModel);
        }

    }
}
