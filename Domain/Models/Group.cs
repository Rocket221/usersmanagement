﻿using Domain.Promises;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class Group : IModelBase
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

    }
}
