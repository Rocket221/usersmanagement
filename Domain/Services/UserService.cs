﻿using Domain.Models;
using Domain.Promises;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class UserService
    {
        protected readonly IRepository<User> repository;

        public UserService(IRepository<User> repo)
        {
            repository = repo;
        }

        public IEnumerable<User> GetAllUsers()
        {
            return repository.GetAll();
        }

        public User Get(Guid id)
        {
            return repository.Get(id);
        }

        public void Save(User user)
        {
            repository.Save(user);
            repository.SaveChanges();

        }

        public void Delete(Guid id)
        {
            repository.Remove(repository.Get(id));
            repository.SaveChanges();
        }
    }
}
