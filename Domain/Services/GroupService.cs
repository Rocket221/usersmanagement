﻿using Domain.Models;
using Domain.Promises;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class GroupService
    {
        protected readonly IRepository<Group> repository;

        public GroupService(IRepository<Group> repo)
        {
            repository = repo;
        }

        public IEnumerable<Group> GetAll()
        {
            return repository.GetAll();
        }

        public Group Get(Guid id)
        {
            return repository.Get(id);
        }

        public void Save(Group group)
        {
            repository.Save(group);
            repository.SaveChanges();

        }

        public void Delete(Guid id)
        {
            repository.Remove(repository.Get(id));
            repository.SaveChanges();
        }
    }
}
