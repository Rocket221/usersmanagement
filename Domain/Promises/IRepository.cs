﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Promises
{
    public interface IRepository<T> where T : class, IModelBase
    {
        T Get(Guid id);
        IEnumerable<T> GetAll();

        void Save(T entity);

        void Remove(T entity);

        void SaveChanges();
    }
}
